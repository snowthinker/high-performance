package org.snowman.test.learn2.chapter2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.snowman.learn2.chapter2.singleton.DoubleCheckedSingleton;
import org.snowman.learn2.chapter2.singleton.SarSingleton;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 上午10:32:58
 */
public class SingletonTest extends TestCase {

	/*@Test
	public void testCreate() {
		Singleton.createString();
	}*/
	
	@Test
	public void testPerformance() throws InterruptedException, ExecutionException, TimeoutException {
		long beginTime = System.currentTimeMillis();
		
		List<Future<Long>> futureTasks = new ArrayList<Future<Long>>();
		
		ExecutorService exectorService = Executors.newFixedThreadPool(10);
		for(int i=0; i<5; i++) {
			Future<Long> futureTask = exectorService.submit(new TestSingleton(beginTime));
			futureTasks.add(futureTask);
		}
		
		for(Future<Long> task : futureTasks) {
			System.out.println("Thread cost: " + task.get(1000, TimeUnit.SECONDS));	
		}
		
		
		Thread.sleep(1000 * 60);
	}
	
	
	class TestSingleton implements Callable<Long> {
		
		private long beginTime;
		
		public TestSingleton(long beginTime) {
			this.beginTime = beginTime;
		}

		@Override
		public Long call() throws Exception {
			for(int i=0; i<100000; i++) {
//				Singleton.getInstance();
//				LazySingleton.getInstance();
				DoubleCheckedSingleton.getInstance();
//				StaticSingleton.getInstance();
			}
			
			Long cost = System.currentTimeMillis() - beginTime;
			
			return cost;
		}
		
	}
	
	
	@Test
	public void testSarSingleton() throws IOException, ClassNotFoundException {
		SarSingleton s1 = null;
		SarSingleton s = SarSingleton.getInstance();
		
		FileOutputStream fos = new FileOutputStream("D:/SarSingleton.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(s);
		oos.flush();
		oos.close();
		
		FileInputStream fis = new FileInputStream("D:/SarSingleton.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		s1 = (SarSingleton) ois.readObject();
		
		Assert.assertEquals(s, s1);
		
	}
}
