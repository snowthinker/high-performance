package org.snowman.test.learn2.chapter2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import junit.framework.TestCase;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月26日 上午11:57:03
 */
public class VolatileTest2 extends TestCase {

	public void testConcurrentIncrease() {
		VolatileTest volatileTest = VolatileTest.getInstance();
		
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		
		for(int i=0; i<5; i++) {

			executorService.execute(new Runnable() {

				@Override
				public void run() {
					for(int i=0; i<1000; i++) {

						volatileTest.count1 += 1;
						Integer count = volatileTest.count.incrementAndGet();
						
						System.out.printf("atomic: %s %d \r\n", Thread.currentThread().getName(), count);	
						System.out.printf("volatile: %s %d \r\n", Thread.currentThread().getName(), volatileTest.count1);
					}
				}
			});	
		}
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		executorService.shutdown();
	}
}
