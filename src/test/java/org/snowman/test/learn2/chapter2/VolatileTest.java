package org.snowman.test.learn2.chapter2;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月26日 上午11:54:04
 */
public class VolatileTest {

	private static VolatileTest instance = null;
	
	public AtomicInteger count = new AtomicInteger(0);
	
	public volatile Integer count1 = 0;
	
	private VolatileTest() {
		
	}
	
	public static VolatileTest getInstance() {
		if(null != instance) {
			return instance;
		}
		
		synchronized(VolatileTest.class) {
			if(null == instance) {
				instance = new VolatileTest();
				return instance;
			}
			
			return instance;
		}
	}
}
