package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月4日 上午9:14:25
 */
public class ExchangerTest {
	
	final static ExecutorService executorService = Executors.newCachedThreadPool();

	final static Exchanger<String> exchanger = new Exchanger<String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final Random random = new Random();
		
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				String data = "Apple";
				System.out.printf("线程%s正在准备交换数据\r\n", Thread.currentThread().getName());
				
				try {
					Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
					String exchangeData = exchanger.exchange(data);
					System.out.printf("线程 %s 换回数据: %s\r\n", Thread.currentThread().getName(), exchangeData);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		executorService.execute(new Runnable() {

			@Override
			public void run() {
				String data = "US Dollar";
				System.out.printf("线程%s正在准备交换数据\r\n", Thread.currentThread().getName());
				
				try {
					Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
					String exchangeData = exchanger.exchange(data);
					System.out.printf("线程 %s 换回数据: %s\r\n", Thread.currentThread().getName(), exchangeData);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		executorService.shutdown();
	}

}
