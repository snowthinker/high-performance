package org.snowman.thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月1日 下午12:44:53
 */
public class ConditionCommunication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Business business = new Business();
		
		new Thread(){
			public void run() {
				for(int i=1; i<=100; i++) {
					business.sub(i);
				}		
			}
		}.start();
		
		
		/*new Thread() {
			public void run() {*/
				for(int i=1; i<=50; i++) {
					business.main(i);
				}
			/*}
		}.start();*/
	}
	
	static class Business {
		private boolean bShouldSub = true;
		
		private Lock lock = new ReentrantLock();
		Condition condition = lock.newCondition();
		
		public void sub(int i) {
			lock.lock();
			
			try {
				while(!bShouldSub) {
					condition.await();	
				}
				
				for(int j=0; j<=10; j++) {
					System.out.println("sub thread sequence of " + j + ", loop of " +i);
				}
				
				bShouldSub = false;
				condition.signal();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
			
		}
		
		public synchronized void main(int i) {
			lock.lock();
			
			try {
				while(bShouldSub) {
					try {
						condition.await();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				for(int j=1; j<=100; j++) {
					System.out.println("main thread sequence of " + j + ", loop of " + i);
				}
				
				bShouldSub = true;
				condition.signal();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

}
