package org.snowman.thread;

import java.util.Random;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月2日 下午3:37:12
 */
public class ThreadLocalTest {
	
	private static Random random = new Random();
	
	public static void main(String[] args) {
		
		for(int i=0; i<2; i++) {

			new Thread(new Runnable() {

				@Override
				public void run() {
					int data = random.nextInt();
					
					MyThreadScopeData scopeData = MyThreadScopeData.getInstance();
					scopeData.setAge(data);
					scopeData.setName(Thread.currentThread().getName());
					
					System.out.printf("%s put data %d \r\n", scopeData.getName(), scopeData.getAge());
					
					new A().get();
					new B().get();
					new C().get();
				}
			}).start();
		}		
	}
	
	static class A {
		public void get() {
			MyThreadScopeData data = MyThreadScopeData.getInstance();
			System.out.printf("Class A read %s data %d \r\n", data.getName(),  data.getAge());
		}
	}
	
	static class B {
		public void get() {
			MyThreadScopeData data = MyThreadScopeData.getInstance();
			System.out.printf("Class B read %s data %d \r\n", data.getName(),  data.getAge());
		}
	}
	
	static class C {
		public void get() {
			MyThreadScopeData data = MyThreadScopeData.getInstance();
			System.out.printf("Class C read %s data %d \r\n", data.getName(),  data.getAge());
		}
	}
}

class MyThreadScopeData {
	
	private MyThreadScopeData() {}
	
	private static ThreadLocal<MyThreadScopeData> map = new ThreadLocal<MyThreadScopeData>();
	
	public static MyThreadScopeData getInstance() {
		MyThreadScopeData instance = map.get();
		if (null == instance) {
			instance = new MyThreadScopeData();
			map.set(instance);
		}

		return instance;
	}
	
	private String name;
	private Integer age;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
}
