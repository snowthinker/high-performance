package org.snowman.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月8日 下午3:23:49
 */
public class CallableAndFuture {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		
		Future<String> futureTask = executorService.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				Thread.sleep(2000);
				return "hello";
			}
		});
		
		try {
			long before = System.currentTimeMillis();
			
			//System.out.printf("%d \r\n", before);
			System.out.println("pending for result");
			String welcome = futureTask.get();
			
			long after = System.currentTimeMillis();
			
			System.out.printf("costs: %d millisecond. result: %s \r\n", (after - before), welcome);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		
		executorService.shutdown();
		
		
		ExecutorService executorService2 = Executors.newFixedThreadPool(10);
		CompletionService<Integer> completionService = new ExecutorCompletionService<Integer>(executorService2);
		
		//final Random random = new Random();
		
		for(int i=0; i<10; i++) {
			
			final int seq = i;
			
			completionService.submit(new Callable<Integer>() {

				@Override
				public Integer call() throws Exception {
					Thread.sleep(Integer.toUnsignedLong( 5000 ));
					return seq;
				}
			});	
		}
		
		
		for(int i=0; i<10; i++) {
			try {
				Integer futureInteger = completionService.take().get();
				System.out.printf("futureInteger: %d \r\n", futureInteger);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		executorService2.shutdown();
		
	}

}
