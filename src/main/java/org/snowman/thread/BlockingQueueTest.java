package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月4日 上午9:42:59
 */
public class BlockingQueueTest {
	
	final static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(3);

	final static Random random = new Random();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for(int i=0; i<2; i++) {
			new Thread() {
				public void run() {
					while(true) {
						try {
							Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
							System.out.printf("线程%s准备放数据\r\n", Thread.currentThread().getName());
							
							queue.put(1);
							
							System.out.printf("线程%s已放了数据，队列还剩%d大小\r\n", Thread.currentThread().getName(), queue.size());
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		}
		
		new Thread(){
			public void run() {
				
			}
		}.start();
	}

}
