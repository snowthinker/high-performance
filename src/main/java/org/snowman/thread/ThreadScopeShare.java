package org.snowman.thread;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月1日 下午1:45:23
 */
public class ThreadScopeShare {
	
	private static Random random = new Random();
	
	private static Map<Thread, Integer> threadData = new ConcurrentHashMap<Thread, Integer>();
	
	
	public static void main(String[] args) {
		
		for(int i=0; i<2; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					int data = random.nextInt();
					System.out.printf("%s has output %d \r\n", Thread.currentThread().getName(),  data);
					
					threadData.put(Thread.currentThread(), data);
					new A().get();
					new B().get();
					new C().get();
				}
				
			}).start();	
		}
	}
	
	static class A {
		public void get() {
			int data = threadData.get(Thread.currentThread());
			System.out.printf("Class A read %s data %d \r\n", Thread.currentThread().getName(),  data);
		}
	}
	
	static class B {
		public void get() {
			int data = threadData.get(Thread.currentThread());
			System.out.printf("Class B read %s data %d \r\n", Thread.currentThread().getName(),  data);
		}
	}
	
	static class C {
		public void get() {
			int data = threadData.get(Thread.currentThread());
			System.out.printf("Class C read %s data %d \r\n", Thread.currentThread().getName(),  data);
		}
	}
}
