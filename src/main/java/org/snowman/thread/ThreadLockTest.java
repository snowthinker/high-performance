package org.snowman.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 下午12:47:07
 */
public class ThreadLockTest {

	public static void main(String[] args) {
		new ThreadLockTest().init();
	}
	
	private void init() {
		final Outputor outputor = new Outputor();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					outputor.output("ZhangSan");
				}
			}
		
		}).start();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					outputor.output3("LiSi");
				}
			}
		
		}).start();
	}
	
	static class Outputor {
		
		Lock lock = new ReentrantLock();

		/**
		 * @param string
		 */
		public void output(String string) {
			
			lock.lock();
			try {
				for(int i=0; i<string.length(); i++) {
					System.out.print(string.charAt(i));
				}
				
				System.out.println();	
			} finally {
				lock.unlock();
			}
		}
		
		public /*synchronized*/ void output2(String string) {
			lock.lock();
			try{
				for (int i = 0; i < string.length(); i++) {
					System.out.print(string.charAt(i));
				}
				System.out.println();
			} finally {
				lock.unlock();
			}	
		}
		
		public /*static*/ /*synchronized*/ void output3(String string) {
			lock.lock();
			
			try{
				for (int i = 0; i < string.length(); i++) {
					System.out.print(string.charAt(i));
				}	
			} finally {
				lock.unlock();
			}
			System.out.println();
		}
		
	}
}
