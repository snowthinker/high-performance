package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月3日 下午9:08:52
 */
public class SemaphoreTest {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newCachedThreadPool();
		final Semaphore semp = new Semaphore(5);
		
		final Random random = new Random();
		
		for(int i=0; i<50; i++) {
			final int no = i;
			
			Runnable r = new Runnable() {

				@Override
				public void run() {
					try {
						semp.acquire();
						System.out.printf("Thread %s accessing\r\n", no);
						
						Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
						
						semp.release();
						
						System.out.printf("Thread %s release\r\n", no);
						
						System.out.printf("Avaliable Permits: %d\r\n", semp.availablePermits());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			
			executorService.execute(r);
		}
		
		executorService.shutdown();
	}
}
