package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月31日 下午10:36:29
 */
public class CacheData {
	
	Object data;
	
	volatile boolean cacheValid;
	
	final ReadWriteLock rw1 = new ReentrantReadWriteLock();
	
	void processData() {
		rw1.readLock().lock();
		
		try{
			if(!cacheValid) {
				rw1.readLock().unlock();
				
				try {
					rw1.writeLock().lock();
					
					data = new Random().nextInt();
					
					rw1.readLock().lock();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					rw1.writeLock().unlock();
				}
			}
			
			System.out.printf("Thread %s read data: %s \r\n", Thread.currentThread().getName(), data);
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			rw1.readLock().unlock();	
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final CacheData cacheData = new CacheData();
		for(int i=0; i<3; i++) {
			new Thread(){
				public void run() {
					while(true) {
						cacheData.processData();	
					}
				}
			}.start();
		}
	}

}
