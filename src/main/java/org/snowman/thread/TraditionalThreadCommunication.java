package org.snowman.thread;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 下午10:19:29
 */
public class TraditionalThreadCommunication {
	

	public static void main(String[] args) throws InterruptedException {
		
		TraditionalThreadCommunication.Business business = new TraditionalThreadCommunication.Business();	
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 5; i++) {
					business.sub(i);
				}
			}
		}).start();

		for (int i = 0; i < 4; i++) {
			business.main(i);
		}

		Thread.sleep(10000);
	}
	
	
	static class Business {
		
		private boolean bShouldSub = true;
		
		public void sub(int i) {
			
			synchronized(this) {
				while(!bShouldSub) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				for(int j=0; j<10; j++) {
					System.out.printf("Sub thread sequence %d,loops %d \r\n", j, i);
				}
				
				this.bShouldSub = false;
				this.notify();
			}
			
		}
		
		public void main(int i) {
			synchronized(this) {
				
				while(this.bShouldSub) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				for(int j=0; j<10; j++) {
					System.out.printf("Main thread sequence %d,loops %d \r\n", j, i);
				}	
				
				this.bShouldSub = true;
				this.notify();
			}
		}
	}
}
