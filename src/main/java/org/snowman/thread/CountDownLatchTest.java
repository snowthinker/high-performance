package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月4日 上午8:42:16
 */
public class CountDownLatchTest {
	
	static ExecutorService executorService = Executors.newCachedThreadPool();
	
	final static CountDownLatch cdOrder = new CountDownLatch(1);
	
	static final CountDownLatch cdAnswer = new CountDownLatch(3);
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		
		for(int i=0; i<3; i++) {
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					try {
						System.out.printf("线程%s正准备接受命令\r\n", Thread.currentThread().getName());
						cdOrder.await();
						
						System.out.printf("线程%s已接受命令\r\n", Thread.currentThread().getName());
						
						Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
						
						System.out.printf("线程%s回应处理命令结果\r\n", Thread.currentThread().getName());
						
						cdAnswer.countDown();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			
			executorService.execute(runnable);
		}
		
		try {
			Thread.sleep(Integer.toUnsignedLong(random.nextInt(10)) * 1000);
			
			System.out.printf("线程%s即将发布命令\r\n", Thread.currentThread().getName());
			
			cdOrder.countDown();
			
			System.out.printf("线程%s已发布命令，正在等待执行结果\r\n", Thread.currentThread().getName());
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		executorService.shutdown();
	}

}
