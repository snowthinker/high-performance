package org.snowman.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月8日 下午1:41:51
 */
public class ThreadPoolTest {
	
	private static final Logger logger = Logger.getLogger(ThreadPoolTest.class.getName());
	
	static {
		//logger.set
	}

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		ExecutorService threadPool = Executors.newFixedThreadPool(20);
//		ExecutorService threadPool = Executors.newCachedThreadPool();
//		ExecutorService threadPool = Executors.newSingleThreadExecutor();
		
		for(int j=0; j<10; j++) {
			threadPool.execute(new Runnable() {

				@Override
				public void run() {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					
					for(int i=0; i<10; i++) {
						String threadName = Thread.currentThread().getName();
						System.out.printf("%s is looping of %d %s \r\n", threadName, i, sdf.format(new Date()));
						
						if((i % 2) == 0 ) {
							/*try {
								//Thread.currentThread().wait();
								//this.wait();
								System.out.printf("%s wait for others \r\n", threadName);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}	*/
						}
						
						//Thread.currentThread().notify();
						//this.notify();
					}
				}
			});
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		System.out.printf("All task submitted, current system millis: %s \r\n", sdf.format(new Date()));
		
		/*threadPool.shutdown();
		boolean isTerminated = threadPool.isTerminated();
		
		System.out.printf("ThreadPool shutdown %s, current system millis: %s \r\n", isTerminated, sdf.format(new Date()));
		*/
		
		Executors.newScheduledThreadPool(3).scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				System.out.println("bombing!");
			}
		}, 3, 2, TimeUnit.SECONDS);
		
		
		threadPool.shutdown();
	}

}
