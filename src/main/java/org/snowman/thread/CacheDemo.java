package org.snowman.thread;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月1日 下午12:13:21
 */
public class CacheDemo {

	private Map<String, Object> cache = new ConcurrentHashMap<String, Object>();
	
	private ReadWriteLock rw = new ReentrantReadWriteLock();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}
	
	public Object getData(String key) {
		
		rw.readLock().lock();
		
		Object obj = null;
		
		try {
			obj = cache.get(key);
			
			if(obj == null) {
				rw.readLock().unlock();
				rw.writeLock().lock();
				
				try {
					if(null == obj) {
						obj = "aaaa";
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					rw.writeLock().unlock();
				}
				
				rw.readLock().lock();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			rw.readLock().unlock();	
		}
		
		return obj;
	}

}
