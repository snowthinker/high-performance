package org.snowman.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 下午12:14:39
 */
public class TimeExample {

	public static void main(String[] args) {
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				System.out.printf("Execute %s \r\n", sdf.format(new Date()));
			}
			},1000, 1000);
	}
}
