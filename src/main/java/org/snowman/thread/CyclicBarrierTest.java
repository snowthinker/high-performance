package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author Andrew-PC
 * @since 2017年2月3日 下午9:46:14
 */
public class CyclicBarrierTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ExecutorService executorService = Executors.newCachedThreadPool();
		
		final CyclicBarrier cb = new CyclicBarrier(3);
		
		final Random random = new Random();
		
		for(int i=0; i<3; i++) {
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(Integer.toUnsignedLong(random.nextInt(10) * 1000));
						System.out.printf("线程%s即将到达集合地点1， 当前已有%d个在等待\r\n", Thread.currentThread().getName(), cb.getNumberWaiting() + 1);
						cb.await();
						
						Thread.sleep(Integer.toUnsignedLong(random.nextInt(10) * 1000));
						System.out.printf("线程%s即将到达集合地点2， 当前已有%d个在等待\r\n", Thread.currentThread().getName(), cb.getNumberWaiting() + 1);
						cb.await();
						
						Thread.sleep(Integer.toUnsignedLong(random.nextInt(10) * 1000));
						System.out.printf("线程%s即将到达集合地点3， 当前已有%d个在等待\r\n", Thread.currentThread().getName(), cb.getNumberWaiting() + 1);
						cb.await();
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (BrokenBarrierException e) {
						e.printStackTrace();
					}
				}
			};
			
			executorService.execute(runnable);
		}
		
		executorService.shutdown();
	}

}
