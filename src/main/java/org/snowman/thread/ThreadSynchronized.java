package org.snowman.thread;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 下午12:47:07
 */
public class ThreadSynchronized {

	public static void main(String[] args) {
		new ThreadSynchronized().init();
	}
	
	private void init() {
		final Outputor outputor = new Outputor();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					outputor.output("ZhangSan");
				}
			}
		
		}).start();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					outputor.output3("LiSi");
				}
			}
		
		}).start();
	}
	
	static class Outputor {

		/**
		 * @param string
		 */
		public void output(String string) {
			synchronized(Outputor.class) {
				for(int i=0; i<string.length(); i++) {
					System.out.print(string.charAt(i));
				}
				
				System.out.println();	
			}
		}
		
		public synchronized void output2(String string) {
			for (int i = 0; i < string.length(); i++) {
				System.out.print(string.charAt(i));
			}
			System.out.println();	
		}
		
		public static synchronized void output3(String string) {
			for (int i = 0; i < string.length(); i++) {
				System.out.print(string.charAt(i));
			}
			System.out.println();
		}
		
	}
}
