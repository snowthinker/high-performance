package org.snowman.thread;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月4日 下午10:41:53
 */
public class MultiThreadShareData {

	public static void main(String[] args) {
		final ShareData shareData1 = new ShareData();
		
		new Thread(new MyRunnable1(shareData1)).start();
		
		new Thread(new MyRunnable2(shareData1)).start();
	}
}

class MyRunnable1 implements Runnable {
	
	private ShareData shareData;
	
	public MyRunnable1(ShareData data1) {
		this.shareData = data1;
	}
	
	
	@Override
	public void run() {
		shareData.increment();
	}
}

class MyRunnable2 implements Runnable {
	
	private ShareData shareData;
	
	public MyRunnable2(ShareData data1) {
		this.shareData = data1;
	}
	
	@Override
	public void run() {
		shareData.decrement();
	}
}


class ShareData /*implements Runnable*/{
	
//	private int count = 100;
	
	private int j =0;
//	
//	@Override
//	public void run() {
//		while(true) {
//			count--;
//			System.out.printf("count: %d \r\n", count);
//		}
//	}
	
	public synchronized void increment() {
		j++;
		System.out.printf("j: %d \r\n", j);
	}
	
	public synchronized void decrement() {
		j--;
		System.out.printf("j: %d \r\n", j);
	}
	
	
}
