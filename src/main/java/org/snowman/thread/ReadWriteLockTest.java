package org.snowman.thread;

import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月31日 下午9:39:51
 */
public class ReadWriteLockTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final Space space = new Space();
		
		Random random = new Random();
		
		for(int i=0; i<3; i++) {
			
			new Thread("Thread Read" + i){
				
				@Override
				public void run() {
					while(true) {
						space.get();
					}
				}
			}.start();
			
			
			new Thread("Thread Write" + i){
				public void run() {
					while(true) {
						space.put(random.nextInt(10000));
					}
				}
			}.start();
		}
		
	}
	
}

class Space {
	private Object data = null;
	
	private ReadWriteLock rw = new ReentrantReadWriteLock();
	
	public void get() {
		
		rw.readLock().lock();
		
		try {
			System.out.printf("%s ready to read \r\n", Thread.currentThread().getName());
			
			Thread.sleep((long)Math.random() * 1000);
			
			System.out.printf("%s finished read data %s \r\n", Thread.currentThread().getName(), data);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			rw.readLock().unlock();	
		}
		
	}
	
	public void put(Object data) {
		
		rw.writeLock().lock();
		
		try {
			System.out.printf("%s ready to write %s \r\n", Thread.currentThread().getName(), data);
			
			Thread.sleep((long)Math.random() * 1000);
			
			this.data = data;
			
			System.out.printf("%s finished write %s \r\n", Thread.currentThread().getName(), data);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			rw.writeLock().unlock();	
		}
		
	}
}

