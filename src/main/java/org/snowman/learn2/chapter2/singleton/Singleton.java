package org.snowman.learn2.chapter2.singleton;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 上午10:29:05
 */
public class Singleton {

	private static Singleton instance = new Singleton();
	
	private Singleton() {
		System.out.println("Singleton is created");
	}
	
	public static Singleton getInstance() {
		return instance;
	}
	
	public static void createString() {
		System.out.println("createString is singleton");
	}
}
