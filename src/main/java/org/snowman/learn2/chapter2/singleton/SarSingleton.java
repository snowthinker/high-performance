package org.snowman.learn2.chapter2.singleton;

import java.io.Serializable;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午2:04:58
 */
public class SarSingleton implements Serializable {

	private static final long serialVersionUID = 4309646324404771016L;
	
	String name;
	
	private SarSingleton() {
		System.out.println("SarSingleton is created");
		name = "SarSingleton";
	}
	
	private static SarSingleton instance = new SarSingleton();
	
	public static SarSingleton getInstance() {
		return instance;
	}
	
	public static void createString() {
		System.out.println("create string is Singleton");
	}
	
	/*public Object readResolve() {
		return instance;
	}*/

}
