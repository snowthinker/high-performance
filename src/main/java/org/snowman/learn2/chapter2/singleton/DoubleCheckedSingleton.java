package org.snowman.learn2.chapter2.singleton;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午1:34:26
 */
public class DoubleCheckedSingleton {

	private static DoubleCheckedSingleton instance = null;
	
	private DoubleCheckedSingleton() {
		System.out.println("DoubleCheckedSingleton is created");
	}
	
	public static DoubleCheckedSingleton getInstance() {
		if (null == instance) {
			synchronized (DoubleCheckedSingleton.class) {
				if(null == instance) {
					instance = new DoubleCheckedSingleton();
				}
			}
		}
		
		return instance;
	}
}
