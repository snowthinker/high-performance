package org.snowman.learn2.chapter2.singleton;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 上午11:08:07
 */
public class StaticSingleton {

	private StaticSingleton() {
		System.out.println("StaticSingleton is created");
	}
	
	private static class SingletonHolder {
		private static StaticSingleton instance = new StaticSingleton();
	}
	
	public static StaticSingleton getInstance() {
		return SingletonHolder.instance;
	}
}
