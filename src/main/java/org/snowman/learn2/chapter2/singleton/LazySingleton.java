package org.snowman.learn2.chapter2.singleton;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 上午11:01:16
 */
public class LazySingleton {

	private static LazySingleton instance = null;
	
	private LazySingleton() {
		System.out.println("LazySingleton is created");
	}
	
	public static synchronized LazySingleton getInstance() {
		if(null == instance) {
			instance = new LazySingleton();
		}
		return instance;
	}
}
