package org.snowman.learn2.chapter2.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午4:28:16
 */
public class JdkDbQueryHandler implements InvocationHandler {
	
	IDBQuery real = null;

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		if(null == real) {
			real = new DBQuery();
		}
		return real.request();
	}

	public static IDBQuery createJdkProxy() {
		IDBQuery jdkProxy = (IDBQuery) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), 
				new Class[]{IDBQuery.class}, new JdkDbQueryHandler());
		
		return jdkProxy;
	}
	
	public static void main(String[] args) {
		IDBQuery dbQuery = JdkDbQueryHandler.createJdkProxy();
		String request = dbQuery.request();
		System.out.println(request);
	}
}
