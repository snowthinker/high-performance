package org.snowman.learn2.chapter2.proxy;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午4:21:51
 */
public class DBQueryProxy implements IDBQuery {
	
	private DBQuery real = null;

	@Override
	public String request() {
		if(null == real) {
			real = new DBQuery();
		}
		return real.request();
	}

	
	public static void main(String[] args) {
		IDBQuery query = new DBQueryProxy();
		String request = query.request();
		System.out.println(request);
	}
}
