package org.snowman.learn2.chapter2.proxy;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午4:38:22
 */
public class CglibDbQueryInterceptor implements MethodInterceptor {

	IDBQuery real = null;
	
	@Override
	public Object intercept(Object arg0, Method arg1, Object[] arg2, MethodProxy arg3) throws Throwable {
		if(null == real) {
			real = new DBQuery();
		}
		return real.request();
	}
	
	
	public static IDBQuery createLibProxy() {
		Enhancer enhancer = new Enhancer();
		
		enhancer.setCallback(new CglibDbQueryInterceptor());
		enhancer.setInterfaces(new Class[] {
				IDBQuery.class
		});
		
		IDBQuery cglibProxy = (IDBQuery) enhancer.create();
		
		return cglibProxy;
	}

	
	
}
