package org.snowman.learn2.chapter2.proxy;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午4:19:09
 */
public interface IDBQuery {

	String request();
	
}
