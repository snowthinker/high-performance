package org.snowman.learn2.chapter2.proxy;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月25日 下午4:20:11
 */
public class DBQuery implements IDBQuery {
	
	public DBQuery() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String request() {
		return "request string";
	}

}
