package org.snowman.learn2.chapter2.flyweight;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月28日 下午10:42:45
 */
public class FinacialReportManager implements IReportManager {

	protected String tenantId = null;
	
	public FinacialReportManager(String tenantId) {
		this.tenantId = tenantId;
	}
	
	@Override
	public String createReport() {
		return "This is a financial Report";
	}

}
