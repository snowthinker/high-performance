package org.snowman.learn2.chapter2.flyweight;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月28日 下午10:45:36
 */
public class EmployeeReportManager implements IReportManager {

	protected String tenantId = null;
	
	public EmployeeReportManager(String tenantId) {
		this.tenantId = tenantId;
	}
	
	@Override
	public String createReport() {
		return "This is a employee report.";
	}

}
