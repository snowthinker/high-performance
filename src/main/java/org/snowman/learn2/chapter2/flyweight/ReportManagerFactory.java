package org.snowman.learn2.chapter2.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月29日 下午9:02:22
 */
public class ReportManagerFactory {

	Map<String, IReportManager> financialReportManager = new HashMap<String, IReportManager>();
	
	Map<String, IReportManager> employeeReportManager = new HashMap<String, IReportManager>();
	
	public IReportManager getFinancialManager(String tanentId) {
		IReportManager r = financialReportManager.get(tanentId);
		if(r == null) {
			r = new FinacialReportManager(tanentId);
			financialReportManager.put(tanentId, r);
		}
		
		return r;
	}
	
	public IReportManager getEmployeeReportManager(String tanentId) {
		IReportManager r = employeeReportManager.get(tanentId);
		if(r == null) {
			r = new EmployeeReportManager(tanentId);
			employeeReportManager.put(tanentId, r);
		}
		
		return r;
	}
	
	public static void main(String[] args) {
		ReportManagerFactory rmf = new ReportManagerFactory();
		IReportManager rm = rmf.getFinancialManager("A");
		System.out.println(rm.createReport());
	}
}
