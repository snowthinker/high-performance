package org.snowman.learn2.chapter2.flyweight;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月28日 下午10:41:47
 */
public interface IReportManager {

	public String createReport();
	
}
