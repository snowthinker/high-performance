package org.snowman.learn2.chapter2.decorate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 上午10:47:47
 */
public class PacketHTTPHeaderCreator extends PacketDecorator {

	/**
	 * @param c
	 */
	public PacketHTTPHeaderCreator(IPacketCreator c) {
		super(c);
	}

	@Override
	public String handleContent() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StringBuffer sb = new StringBuffer();
		sb.append("Cache-Control:no-cache\n");
		sb.append("Date:" + sdf.format(new Date()) + "\n");
		sb.append(component.handleContent());
		
		return sb.toString();
	}

	
	public static void main(String[] args) {
		IPacketCreator pc = new PacketHTTPHeaderCreator(
								new PacketHTMLHeaderCreator(
										new PacketBodyCreator()
								)
							);
		
		System.out.printf("%s \r\n", pc.handleContent());
	}
}
