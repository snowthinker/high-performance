package org.snowman.learn2.chapter2.decorate;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 上午9:30:11
 */
public interface IPacketCreator {

	public String handleContent();
	
}
