package org.snowman.learn2.chapter2.decorate;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 上午9:33:23
 */
public abstract class PacketDecorator implements IPacketCreator {

	IPacketCreator component;
	
	public PacketDecorator(IPacketCreator c) {
		component = c;
	}
	
	@Override
	public String handleContent() {
		return "Content of PackageDecorator";
	}

}
