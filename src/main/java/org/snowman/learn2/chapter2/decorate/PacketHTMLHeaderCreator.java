package org.snowman.learn2.chapter2.decorate;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 上午9:37:15
 */
public class PacketHTMLHeaderCreator extends PacketDecorator {

	/**
	 * @param c
	 */
	public PacketHTMLHeaderCreator(IPacketCreator c) {
		super(c);
	}

	@Override
	public String handleContent() {
		StringBuffer sb = new StringBuffer();
		sb.append("<html>");
		sb.append("<body>");
		sb.append(component.handleContent());
		sb.append("</body>");
		sb.append("</hrml>\n");
		
		return sb.toString();
	}

}
