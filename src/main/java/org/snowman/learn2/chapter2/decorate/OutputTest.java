package org.snowman.learn2.chapter2.decorate;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月3日 下午10:21:43
 */
public class OutputTest {

	public static void main(String[] args) throws IOException {
		DataOutputStream dout = new DataOutputStream(
									new BufferedOutputStream(
										new FileOutputStream("D:\\b.txt")
									)
								);
		
		long begin = System.currentTimeMillis();
		for(int i=0; i<100000; i++) {
			dout.writeLong(i);
		}
		
		System.out.printf("Cost: %d \r\n", (System.currentTimeMillis() - begin));
	}
}
