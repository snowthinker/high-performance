package org.snowman.learn2.chapter2.decorate;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月31日 上午9:30:52
 */
public class PacketBodyCreator implements IPacketCreator {

	@Override
	public String handleContent() {
		return "Content of Package";
	}

}
