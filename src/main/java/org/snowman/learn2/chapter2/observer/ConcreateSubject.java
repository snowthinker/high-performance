package org.snowman.learn2.chapter2.observer;

import java.awt.Event;
import java.util.Vector;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月3日 下午10:46:43
 */
public class ConcreateSubject implements ISubject {
	
	Vector<IObserver> observers =  new Vector<IObserver>();

	@Override
	public void attach(IObserver observer) {
		observers.addElement(observer);
	}

	@Override
	public void detach(IObserver observer) {
		observers.removeElement(observer);
	}

	@Override
	public void inform() {
		//Event ent = new Event();
	}

}
