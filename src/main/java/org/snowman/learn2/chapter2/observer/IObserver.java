package org.snowman.learn2.chapter2.observer;

import java.awt.Event;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月3日 下午10:35:09
 */
public interface IObserver {

	void update(Event evt);
}
