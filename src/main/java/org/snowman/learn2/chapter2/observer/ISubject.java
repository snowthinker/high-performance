package org.snowman.learn2.chapter2.observer;

/**
 * 
 * @author Andrew-PC
 * @since 2017年1月3日 下午10:34:27
 */
public interface ISubject {

	void attach(IObserver observer);
	
	void detach(IObserver observer);
	
	void inform();
}
