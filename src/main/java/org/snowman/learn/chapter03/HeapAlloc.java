package org.snowman.learn.chapter03;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月24日 上午10:32:59
 */
public class HeapAlloc {

	/**
	 * -Xmx20m -Xms5m -XX:+PrintCommandLineFlags -XX:+PrintGCDetails -XX:+UseSerialGC
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.printf("maxMemory: %d KB \r\n", Runtime.getRuntime().maxMemory()/1024);
		
		System.out.printf("free mem: %d KB \r\n", Runtime.getRuntime().freeMemory()/1024);
		
		System.out.printf("total mem: %d KB \r\n", Runtime.getRuntime().totalMemory()/1024);
		
		
		byte[] b = new byte[1 * 1024 * 1024];
		System.out.println("分配了1M的空间给数组");
		
		System.out.printf("maxMemory: %d KB \r\n", Runtime.getRuntime().maxMemory()/1024);
		
		System.out.printf("free mem: %d KB \r\n", Runtime.getRuntime().freeMemory()/1024);
		
		System.out.printf("total mem: %d KB \r\n", Runtime.getRuntime().totalMemory()/1024);
		
		b = new byte[4 * 1024 * 1024];
		System.out.println("分配了4M的空间给数组");
		
		System.out.printf("maxMemory: %d KB \r\n", Runtime.getRuntime().maxMemory()/1024);
		
		System.out.printf("free mem: %d KB \r\n", Runtime.getRuntime().freeMemory()/1024);
		
		System.out.printf("total mem: %d KB \r\n", Runtime.getRuntime().totalMemory()/1024);
		
	}
}
