package org.snowman.learn.chapter01;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月19日 下午9:47:17
 */
public class OnStackTest {

	public static class User {
		public int id = 0;
		public String name = "";
	}
	
	public static void alloc() {
		User u = new User();
		u.id = 5;
		u.name = "geym";
	}
	
	/**
	 * -Xmx10m -Xms10m -XX:+DoEscapeAnalysis -XX:+PrintGC -XX:+UseTLAB  -XX:+EliminateAllocations
	 * @param args
	 */
	public static void main(String[] args) {
		long b = System.currentTimeMillis();
		
		for(int i=0; i<100000000; i++) {
			alloc();
		}
		
		long e = System.currentTimeMillis();
		
		System.out.println(e - b);
	}

}
