package org.snowman.learn.chapter01;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月17日 下午9:57:16
 */
public class LocalVarGc {

	public void localVarGc1() {
		byte[] a = new byte[6 * 1024 * 1024];
		System.gc();
	}
	
	public void localVarGc2() {
		byte[] a = new byte[6 * 1024 * 1024];
		a = null;
		System.gc();
	}
	
	public void localVarGc3() {
		{
			byte[] a = new byte[6 * 1024 * 1024];
		}
		System.gc();
	}
	
	public void localVarGc4() {
		{
			byte[] a = new byte[6 * 1024 * 1024];
		}
		
		int c = 10;
		System.gc();
	}
	
	public void localVarGc5() {
		localVarGc1();
		System.gc();
	}
	
	/**
	 * -XX:+PrintGC
	 * -XX:+PrintGCDetails
	 * @param args
	 */
	public static void main(String[] args) {
		LocalVarGc inst = new LocalVarGc();
		inst.localVarGc2();
	}
}
