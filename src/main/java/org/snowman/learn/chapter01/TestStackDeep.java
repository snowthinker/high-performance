package org.snowman.learn.chapter01;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月17日 下午9:21:10
 */
public class TestStackDeep {

	private static int count = 0;
	
	public static void recursion(long a, long b, long c) {
		long e = 1, f = 2, g = 3, h = 4, i = 5, j = 6, k = 7, x = 8, y = 9, z = 10;
		count ++;
		recursion(a, b, c);
	}
	
	public static void recursion() {
		count ++;
		recursion();
	}
	
	/**
	 * -Xss128K
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//recursion();
			recursion(0L, 0L, 0L);
		} catch (Throwable e) {
			System.out.println("deep of calling = " + count);
			e.printStackTrace();
		}
	}
}
