package org.snowman.learn.chapter01;

/**
 * 
 * @author Andrew-PC
 * @since 2016年12月19日 下午9:26:57
 */
public class Binary {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		float a = -5;
		System.out.println(Integer.toBinaryString(Float.floatToIntBits(a)));
		
		System.out.println(Integer.toBinaryString(-5));
	}

}
